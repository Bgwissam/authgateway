package com.team3.authgateway.database;
/**
 restful_api will control adding, updating, removing all users with their features.
 The class will host several methods:
 addUser: will add a new user with the assigned features
 updateUser: will update a current user depending on the id provided for the specific user,
 deleteUser: will be used to delete a current user
 getUser: will get the data associated with a certain user
 getAllUsers: will get all the users in the database
 */
public class restful_api {
}
